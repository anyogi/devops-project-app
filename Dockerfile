FROM python:3.9.18

WORKDIR /src/ap

COPY . .

RUN pip install -r requirements.txt

ENV PYTHONUNBUFFERED=1

CMD [ "python", "./main.py"]