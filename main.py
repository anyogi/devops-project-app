import requests
import json

#city = input("Please enter the city: ")
city = "Mumbai"

response_API = requests.get('https://api.covid19india.org/state_district_wise.json')

data = response_API.text
parse_json = json.loads(data)

active_case = parse_json['Maharashtra']['districtData'][ city ]['active']

print ( "City:", city )
print ("Active cases:", active_case)